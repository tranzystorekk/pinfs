#include "messages/FindValueMsg.hpp"

#include "global.h"
#include "utility.hpp"

using namespace pinfs::messages;

std::string FindValueRequest::serialize() {
  std::string ret;
  const int size = msgHeaderSize()
    + sizeof(pinfs::Key);

  ret += makeHeader(size);
  ret.append(reinterpret_cast<const char*>(searched.bytes.data()), PINFS_KEY_LENGTH);

  return ret;
}

std::string FindValueResponse::serialize() {
  std::string ret;
  int size = msgHeaderSize();

  switch(rtype) {
  case ResponseType::NOFILE:
    size += PINFS_CHAR_FIELD_WIDTH + nearest.size() * (PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH + PINFS_KEY_LENGTH);
    ret += makeHeader(size);
    ret += (char)(rtype);
    for(auto& contact : nearest) {
      ret.append(reinterpret_cast<const char*>(utility::ToBytes<unsigned long, PINFS_LONG_FIELD_WIDTH>(contact.address.ip_address).data()),
                 PINFS_LONG_FIELD_WIDTH);
      ret.append(reinterpret_cast<const char*>(utility::ToBytes<unsigned short, PINFS_SHORT_FIELD_WIDTH>(contact.address.port).data()),
                 PINFS_SHORT_FIELD_WIDTH);
      ret.append(reinterpret_cast<const char*>(contact.id.bytes.data()),
                 PINFS_KEY_LENGTH);
    }
    break;

  case ResponseType::FILE:
    size += PINFS_CHAR_FIELD_WIDTH + filecontent.size();
    ret += makeHeader(size);
    ret += (char)(rtype);
    ret.append(reinterpret_cast<const char*>(filecontent.data()), filecontent.size());
    break;
  }

  return ret;
}
