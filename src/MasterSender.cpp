#include "threadobjects/MasterSender.hpp"

#include "global.h"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <functional>

using namespace boost::asio;
using namespace boost::posix_time;

using namespace std::placeholders;
using namespace pinfs::threadobjects;

void AtomicThreadCounter::incrementIfLessThan(int val) {
  std::unique_lock<std::mutex> lock(mutex_);

  cv_.wait(lock,
           [this, val]() {
             return counter_ < val;
           });

  ++counter_;
}

void AtomicThreadCounter::decrement() {
  std::unique_lock<std::mutex>(mutex_);

  --counter_;
  cv_.notify_one();
}


void MasterSender::addToQueue(messages::Message* m, const pinfs::Address& address) {
  queue_.push(MsgInfo(m, address));
}

void MasterSender::executor() {
  while(1) {
    MsgInfo msg = queue_.pop();

    std::cout << "Sending to address " << std::hex <<msg.receiver.ip_address << std::dec << " port " << msg.receiver.port << std::endl;

    tcounter_.incrementIfLessThan(PINFS_ALPHA_FACTOR);

    std::thread sender_worker(senderWorker, std::ref(ioc_), msg.message->serialize(), msg.receiver, std::ref(tcounter_));
    sender_worker.detach();
  }
}

void closeSocket(const boost::system::error_code& ec, ip::tcp::socket& socket) {
  if(ec != boost::asio::error::operation_aborted)
    socket.close();
}

void handleConnect(const boost::system::error_code& error,
                   boost::system::error_code& dest) {
  dest = error;
}

void pinfs::threadobjects::senderWorker(io_context& ioc, std::string msg, pinfs::Address address, AtomicThreadCounter& counter) {
  deadline_timer deadline(ioc);
  ip::tcp::socket socket(ioc);
  ip::tcp::endpoint ep(ip::address_v4(address.ip_address), address.port);

  deadline.expires_from_now(minutes(1));
  deadline.async_wait(std::bind(closeSocket, _1, std::ref(socket)));

  boost::system::error_code ec = boost::asio::error::would_block;

  socket.async_connect(ep, std::bind(handleConnect, _1, std::ref(ec)));

  //block until connected
  do ioc.run_one(); while(ec == boost::asio::error::would_block);
  deadline.cancel();

  if(!ec) {
    boost::system::error_code write_error;

    boost::asio::write(socket, boost::asio::buffer(msg), write_error);

    if(write_error)
      std::cout << "Send error: " << write_error.message() << std::endl;
  }

  counter.decrement();
}
