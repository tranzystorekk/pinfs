#include "global.h"
#include "messages/PingMsg.hpp"

#include "Contact.hpp"

#include "utility.hpp"

using namespace pinfs::messages;

std::string PingRequest::serialize() {
  return makeHeader(msgHeaderSize());
}

std::string PingResponse::serialize() {
  return makeHeader(msgHeaderSize());
}
