#include "threadobjects/Controller.hpp"

#include <boost/filesystem.hpp>

#include "events/GetEvent.hpp"
#include "events/PutEvent.hpp"

using namespace pinfs::threadobjects;

void Controller::executor(){
  std::string request;

  while(1) {
    std::getline(std::cin, request);

    pinfs::events::Event* event = parseTextToEvent(request);
    if (event != nullptr) {
      manager_.addToQueue(event);
      ControlSynchronizer::Future future = cs_.getFuture();

      std::cout << future.get() << std::endl;
    }
    else
      std::cout<<"This file does not exist!"<<std::endl;
  }
}

pinfs::events::Event* Controller::parseTextToEvent(const std::string& commandText){
  std::string commandName, filename;
  std::stringstream stream(commandText);
  getline(stream, commandName, ' ');
  getline(stream, filename, ' ');

  boost::filesystem::path path(filename);

  if(boost::filesystem::exists(path) && boost::filesystem::is_regular_file(path)) {
    if(commandName == "put")
      return new pinfs::events::PutEvent(filename);
  }

  if(commandName == "get")
    return new pinfs::events::GetEvent(filename);

  return nullptr;
}
