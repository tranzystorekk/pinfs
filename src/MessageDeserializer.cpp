#include "messages/MessageDeserializer.hpp"

#include "global.h"
#include "utility.hpp"

#include "Contact.hpp"

#include "messages/Message.hpp"
#include "messages/InvalidMsg.hpp"
#include "messages/PingMsg.hpp"
#include "messages/ExistsMsg.hpp"
#include "messages/StoreMsg.hpp"
#include "messages/FindNodeMsg.hpp"
#include "messages/FindValueMsg.hpp"

using namespace pinfs::messages;

Message* MessageDeserializer::Deserialize(const std::string& msgstring) {
  //TODO implementation

  if(msgstring.size() < 9) {
    return new Invalid;
  }

  int msgSize = utility::FromStringPartial<int, PINFS_INT_FIELD_WIDTH>(msgstring, 0);

  // std::cout << msgstring.size() << " " << msgSize << std::endl;

  if(msgSize != msgstring.size()) {
    return new Invalid;
  }

  int msgId = utility::FromStringPartial<int, PINFS_INT_FIELD_WIDTH>(msgstring, 4);

  Message::Type msgType = utility::FromStringPartial<Message::Type>(msgstring, 8);

  // std::cout << msgSize << " " << msgId << " " << msgType << std::endl;

  unsigned long ipAddress = utility::FromStringPartial<unsigned long, PINFS_LONG_FIELD_WIDTH>(msgstring, 9);
  unsigned short port = utility::FromStringPartial<unsigned short, PINFS_SHORT_FIELD_WIDTH>(msgstring, 17);

  pinfs::Key key = pinfs::Key::FromByteChunk(msgstring, 9 + PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH);

  // std::cout << key.toHexString() << "\n" << ipAddress << "\n" << port << std::endl;

  const int bodySize = msgSize - msgHeaderSize();

  switch(msgType) {
  case Message::PING_REQ:
    return new PingRequest(msgId, pinfs::Contact(ipAddress, port, key));

  case Message::PING_RES:
    return new PingResponse(msgId, pinfs::Contact(ipAddress, port, key));

  case Message::STORE_REQ:
    {
      StoreRequest* result = new StoreRequest(msgId, pinfs::Contact(ipAddress, port, key));
      const int filenameLength = utility::FromStringPartial<int, PINFS_INT_FIELD_WIDTH>(msgstring, msgHeaderSize());

      const int fnameEnd = msgHeaderSize() + PINFS_INT_FIELD_WIDTH + filenameLength;
      const int msgEnd = msgstring.size();

      int i=msgHeaderSize() + PINFS_INT_FIELD_WIDTH;
      for(; i<fnameEnd; ++i) {
        result->filename += msgstring[i];
      }

      for(; i<msgEnd; ++i) {
        std::cout << msgstring[i] << " ";
        result->filecontent.push_back(msgstring[i]);
      }

      std::cout << std::endl;

      return result;
    }

  case Message::STORE_RES:
    return new StoreResponse(msgId, pinfs::Contact(ipAddress, port, key));

  case Message::EXISTS_REQ:
    {
      ExistsRequest* result = new ExistsRequest(msgId, pinfs::Contact(ipAddress, port, key));
      result->fileId = pinfs::Key::FromByteChunk(msgstring, msgHeaderSize());

      return result;
    }

  case Message::EXISTS_RES:
    {
      ExistsResponse* result = new ExistsResponse(msgId, pinfs::Contact(ipAddress, port, key));
      result->fileExists = utility::FromStringPartial<bool>(msgstring, msgHeaderSize());

      return result;
    }

  case Message::FNODE_REQ:
    {
      FindNodeRequest* result = new FindNodeRequest(msgId, pinfs::Contact(ipAddress, port, key));
      result->searched = pinfs::Key::FromByteChunk(msgstring, msgHeaderSize());

      return result;
    }

  case Message::FNODE_RES:
    {
      FindNodeResponse* result = new FindNodeResponse(msgId, pinfs::Contact(ipAddress, port, key));
      const int fieldSize = PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH + PINFS_KEY_LENGTH;
      const int keynumber = bodySize / fieldSize;

      std::cout << "Deserializer report FNODE_REPONSE delivered " << keynumber << " keys." << std::endl;

      int i=0;
      while(i < keynumber) {
        result->nearest.emplace_back(utility::FromStringPartial<unsigned long, PINFS_LONG_FIELD_WIDTH>(msgstring,
                                                                                                       msgHeaderSize() + i * fieldSize),
                                     utility::FromStringPartial<unsigned short, PINFS_SHORT_FIELD_WIDTH>(msgstring,
                                                                                                         msgHeaderSize()
                                                                                                         + i * fieldSize + PINFS_LONG_FIELD_WIDTH),
                                     pinfs::Key::FromByteChunk(msgstring,
                                                               msgHeaderSize() + i * fieldSize + PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH));

        ++i;
      }

      return result;
    }

  case Message::FVALUE_REQ:
    {
      FindValueRequest* result = new FindValueRequest(msgId, pinfs::Contact(ipAddress, port, key));
      result->searched = pinfs::Key::FromByteChunk(msgstring, msgHeaderSize());

      return result;
    }

  case Message::FVALUE_RES:
    {
      FindValueResponse* result = new FindValueResponse(msgId, pinfs::Contact(ipAddress, port, key));
      result->rtype = utility::FromStringPartial<FindValueResponse::ResponseType>(msgstring, msgHeaderSize());

      switch(result->rtype) {
      case FindValueResponse::NOFILE:
        {
          const int fieldSize = PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH + PINFS_KEY_LENGTH;
          const int keynumber = (bodySize - 1) / fieldSize;

          int i=0;
          while(i < keynumber) {
            result->nearest.emplace_back(utility::FromStringPartial<unsigned long, PINFS_LONG_FIELD_WIDTH>(msgstring,
                                                                                                           msgHeaderSize() + PINFS_CHAR_FIELD_WIDTH
                                                                                                           + i * fieldSize),
                                         utility::FromStringPartial<unsigned short, PINFS_SHORT_FIELD_WIDTH>(msgstring,
                                                                                                             msgHeaderSize() + PINFS_CHAR_FIELD_WIDTH
                                                                                                             + i * fieldSize + PINFS_LONG_FIELD_WIDTH),
                                         pinfs::Key::FromByteChunk(msgstring,
                                                                   msgHeaderSize() + PINFS_CHAR_FIELD_WIDTH
                                                                   + i * fieldSize + PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH));

            ++i;
          }
        }
        break;

      case FindValueResponse::FILE:
        for(int i=msgHeaderSize() + PINFS_CHAR_FIELD_WIDTH; i<msgSize; ++i) {
          result->filecontent.push_back(msgstring[i]);
        }
        break;
      }

      return result;
    }

  default:
    return new Invalid;
  }
}
