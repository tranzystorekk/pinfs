#include <iostream>

#include <boost/asio.hpp>
#include <boost/filesystem.hpp>

#include <thread>
#include <functional>

#include "cxxopts.hpp"

#include "Contact.hpp"
#include "threadobjects/MasterReceiver.hpp"
#include "threadobjects/MasterSender.hpp"
#include "threadobjects/Controller.hpp"
#include "threadobjects/ControlSynchronizer.hpp"

#include "messages/PingMsg.hpp"
#include "global.h"

using namespace pinfs::threadobjects;

unsigned long ipToLong(std::string ip_address){
        std::string word;
        unsigned long result = 0;
        std::stringstream stream(ip_address);
        int i = 3;
        while( getline(stream, word, '.') ){
                unsigned long ip = std::stoul(word);
                result |= ip << (i * 8);
                --i;
        }
        return result;
}

pinfs::Address stringToAddress (std::string address){
    std::string output_address = "";
    std::string temp = "";
    int port = -1;

    for(char& c : address) {
          temp += c;
          if(c == ':') {
            output_address = temp;
            temp = "";
          }
    }
    if (output_address == "")
        output_address = temp;
    else
        port = std::stoi(temp);
    if (port == -1){
        port = DEFAULT_PORT;
    }

    return pinfs::Address(ipToLong(output_address), port);
}


int main(int argc, char** argv) {
  pinfs::Address address;
  pinfs::Address bootstrapAddress;
  std::string storepath;

  bool isBootstrap = false;

  try {
    const std::string description =
      "PINFS (PINFS Is Not a File System) is a project of simplified dispersed file system.\n"
      "Basing on IPFS, the system connects clients as nodes of P2P network and enables sharing files placed in network of one of them.\n"
      "System is decentralized - neither of nodes in the network is favoured in any role or function.\n"
      "More info about IPFS: https://ipfs.io/\nmore info about Kademlia: https://en.wikipedia.org/wiki/Kademlia\n" ;

    cxxopts::Options options("pinfs", description);

    options
      .add_options()
      ("b,bootstrap", "Bootstrap node IP address", cxxopts::value<std::string>())
      ("a,myaddress", "IP address of this node", cxxopts::value<std::string>())
      ("p,storepath", "Path for storing network files", cxxopts::value<std::string>())
      ("h,help", "Print help")
      ;

    auto result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cout << options.help({""}) << std::endl;
      return (0);
    }

    if(result.count("myaddress")) {
      std::string myaddress = result["myaddress"].as<std::string>();
      address = stringToAddress(myaddress);
    }
    else {
      std::cerr << "error: -a option is required" << std::endl;
      return 1;
    }

    if(result.count("bootstrap")) {
      std::string bootstrap = result["bootstrap"].as<std::string>();
      bootstrapAddress = stringToAddress(bootstrap);
      isBootstrap = true;
    }

    if(result.count("storepath")) {
      storepath = result["storepath"].as<std::string>();

      boost::filesystem::path path(storepath);
      if(!boost::filesystem::exists(path) || !boost::filesystem::is_directory(path)) {
        std::cerr << "Supplied store path is not a directory" << std::endl;
        return 1;
      }

      if(storepath.back() != '/')
        storepath += '/';
    }
  }
  catch (const cxxopts::OptionException& e) {
    std::cerr << "error parsing options: " << e.what() << std::endl;
    return 1;
  }

  boost::asio::io_context ioc;
  pinfs::Contact meContact(address.ip_address, address.port,
                           pinfs::Key::GenerateRandom());
  ControlSynchronizer cs;

  MasterSender ms(ioc);
  Manager manager(ms, meContact, ioc, cs, storepath,
                  (isBootstrap ? Bootstrap(bootstrapAddress)
                   : Bootstrap()));
  MasterReceiver mr(manager);
  Controller controller(manager, cs);

  std::thread master_recv(&MasterReceiver::executor, std::ref(mr));
  std::thread manager_thread(&Manager::executor, std::ref(manager));
  std::thread master_send(&MasterSender::executor, std::ref(ms));
  std::thread recv_worker(receiverWorker, std::ref(mr), std::ref(ioc), address.port);
  std::thread controller_thread(&Controller::executor, std::ref(controller));

  controller_thread.join();
  recv_worker.join();
  master_send.join();
  manager_thread.join();
  master_recv.join();

  return 0;
}
