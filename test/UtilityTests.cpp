#include "utility.hpp"
#include "catch.hpp"

#include <iostream>

TEST_CASE( "types are aligned correctly when truncated" ) {
  auto bytes = pinfs::utility::ToBytes<int, 3>(10);

  REQUIRE(bytes[0] == 10);
  REQUIRE(bytes[1] == 0);
  REQUIRE(bytes[2] == 0);
}

TEST_CASE( "types are aligned correctly when padded" ) {
  auto bytes = pinfs::utility::ToBytes<int, 8>(10);

  REQUIRE(bytes[0] == 10);
  REQUIRE(bytes[1] == 0);
  REQUIRE(bytes[2] == 0);
  REQUIRE(bytes[3] == 0);
  REQUIRE(bytes[4] == 0);
  REQUIRE(bytes[5] == 0);
  REQUIRE(bytes[6] == 0);
  REQUIRE(bytes[7] == 0);
}
