#include "concurrent/SynchronizedQueue.hpp"
#include "catch.hpp"

#include <iostream>
#include <functional>
#include <thread>

void producer(pinfs::concurrent::SynchronizedQueue<std::string>& q) {
  std::string a("Object");

  for(int i=0; i<10; ++i) {
    q.push(a + std::to_string(i+1));
  }
}

void consumer(pinfs::concurrent::SynchronizedQueue<std::string>& q, bool& success) {
  int i=0;
  for(int i=0; i<10; ++i) {
    q.pop();
  }

  success = true;
}

TEST_CASE( "" ) {
  pinfs::concurrent::SynchronizedQueue<std::string> queue;
  bool isSuccess = false;

  std::thread prod(producer, std::ref(queue));
  std::thread cons(consumer, std::ref(queue), std::ref(isSuccess));

  prod.join();
  cons.join();

  REQUIRE(isSuccess);
}
