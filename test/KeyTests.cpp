#include "Key.hpp"
#include "catch.hpp"

#include <cstring>

#include <thread>

#include "hashlib++/hashlibpp.h"
#include "hashlib++/hl_sha1wrapper.h"

pinfs::Key keys[2];

void testGeneratorTread(int pos) {
  keys[pos] = pinfs::Key::GenerateRandom();
}

TEST_CASE( "sha1 works" ) {
  sha1wrapper myWrapper;
  bool isSuccess = false;

  try {
    myWrapper.test();
    isSuccess = true;
  }
  catch(hlException& e) {
    std::cerr << e.error_message() << std::endl;
  }

  REQUIRE(isSuccess);
}

TEST_CASE( "xor of two keys is calculated correctly", "[key]" ) {
  pinfs::Key a("abcdefghijklmnopqrst");
  pinfs::Key b("ABCDEFGHIJKLMNOPQRST");

  REQUIRE((a ^ b) ==
          pinfs::Key
          ::FromHexString("2020202020202020202020202020202020202020"));
}

TEST_CASE( "zero-prefix length of a key is calculated correctly", "[key]" ) {
  pinfs::Key k = pinfs::Key
    ::FromHexString("0000006000000000000000000000000000000000");
  //0000 0000 0000 0000 0000 0000 0110 ...
  //0    4    8    12   16   20   24

  CAPTURE(k);

  REQUIRE(k.getPrefixLength() == 25);
}

TEST_CASE( "when generated from SHA1 hash, the key is correct" ) {
  pinfs::Key k = pinfs::Key::FromString("ABCD");
  sha1wrapper wrapper;

  std::string hashed = wrapper.getHashFromString("ABCD");

  REQUIRE(k.toHexString() == hashed);
}

TEST_CASE( "when multiple threads generate key, the generated keys are unique", "[key]" ) {
  std::thread thread1(testGeneratorTread, 0);
  std::thread thread2(testGeneratorTread, 1);

  thread1.join();
  thread2.join();

  REQUIRE(keys[0] != keys[1]);
}
