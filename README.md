# PINFS

PINFS (PINFS Is Not a File System) is a project of a simplified dispersed file system.
Based on IPFS, the system connects clients as nodes of P2P network and enables sharing files placed in network between each other.
System is decentralized - no node in the network is favoured in any role or function.

## Technologies

Project is written in C++ language, but fragments responsible for network transport are using C/Sockets API.
Project also uses additional external libraries, such as Catch2 - for unit tests, cxxopts - to read arguments from command line, etc. 
Hashlib2plus library provides SHA1 hashes to hash filenames.

## Transport layer
Transfer of data in both directions via the TCP protocol.

## Build

Type `scons` to build app

## Run 

Execute script in 5 modes:

`run.sh:`

* without parameters and then select ip address port and bootstrap address 
* with ip address
* with ip address and path to directory
* with ip, directory, and bootstrap ip address
* with --help option to see available options
 
 Examples:
    
*   ./run.sh
*   ./run.sh 192.168.0.1:40500
*   ./run.sh 192.168.0.1 ~/filesystem
*   ./run.sh 192.168.0.1 ~/filesystem 192.168.0.2
*   ./run.sh 192.168.0.1:50001 ~/filesystem 192.168.0.2:50002
*   ./run.sh --help 

## Tests

Type `scons test` to run tests

## Authors

* Komorowski Piotr
* Puc Marcin
* Pleban Jakub
* Wasiak Mateusz
