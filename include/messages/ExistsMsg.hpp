#ifndef PINFS_EXISTS_MSG_HPP
#define PINFS_EXISTS_MSG_HPP

#include "messages/Message.hpp"
#include "Key.hpp"

namespace pinfs {
namespace messages {

struct ExistsRequest : public Message {
  Key fileId;

  ExistsRequest(int id, const Contact& contact)
    : Message(Message::EXISTS_REQ, id, contact) {
  }

  std::string serialize() override;
};

  struct ExistsResponse : public Message {
  bool fileExists;

  ExistsResponse(int id, const Contact& contact, bool exists = false)
    : Message(Message::EXISTS_RES, id, contact),
      fileExists(exists) {
  }

  std::string serialize() override;
};

} //messages
} //pinfs

#endif //PINFS_EXISTS_MSG_HPP
