#ifndef PINFS_FIND_NODE_MSG_HPP
#define PINFS_FIND_NODE_MSG_HPP

#include "Contact.hpp"
#include "messages/Message.hpp"

#include <vector>

namespace pinfs {
namespace messages {

struct FindNodeRequest : public Message {
  Key searched;

  FindNodeRequest(int id, const Contact& contact)
    : Message(Message::FNODE_REQ, id, contact) {
  }

  std::string serialize() override;
};

struct FindNodeResponse : public Message {
  std::vector<Contact> nearest;

  FindNodeResponse(int id, const Contact& contact)
    : Message(Message::FNODE_RES, id, contact) {
  }

  std::string serialize() override;
};

} //messages
} //pinfs

#endif //PINFS_FIND_NODE_MSG_HPP
