#ifndef PINFS_INVALID_MSG_HPP
#define PINFS_INVALID_MSG_HPP

#include "messages/Message.hpp"

namespace pinfs {
namespace messages {

struct Invalid : public Message {
  Invalid()
    : Message(Type::INVALID) {
  }

  std::string serialize() override {
    return std::string();
  }
};

} //messages
} //pinfs

#endif //PINFS_INVALID_MSG_HPP
