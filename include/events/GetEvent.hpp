#ifndef PINFS_GETEVENT_HPP
#define PINFS_GETEVENT_HPP

#include "events/Event.hpp"

namespace pinfs {
namespace events {

struct GetEvent : public Event {
  std::string fileName;

  GetEvent(const std::string& fname)
    : Event(Event::GET),
      fileName(fname) {
  }
};

} //events
} //pinfs

#endif //PINFS_GETEVENT_HPP
