#ifndef PINFS_KEY_HPP
#define PINFS_KEY_HPP

#include "global.h"

#include <iostream>
#include <string>
#include <array>

namespace pinfs {

struct Key {
  using Byte = unsigned char;

  std::array<Byte, PINFS_KEY_LENGTH> bytes;

  Key() = default;
  Key(const char* str);
  Key(const Key& other);

  Key& operator=(const char* str);
  Key& operator=(const Key& other);

  bool operator==(const Key& other) const;
  bool operator!=(const Key& other) const;
  bool operator<(const Key& other) const;

  Key operator^(const Key& other) const;

  int getPrefixLength() const;

  std::string toHexString() const;

  static Key FromByteChunk(const std::string& str, int pos);
  static Key FromHexString(const std::string& hexstr);
  static Key FromString(const std::string& unhashed);

  static Key GenerateRandom();
};

std::ostream& operator<<(std::ostream& os, const Key& k);

} //pinfs

#endif //PINFS_KEY_HPP
