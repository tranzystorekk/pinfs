#ifndef PINFS_CONTACT_HPP
#define PINFS_CONTACT_HPP

#include "Key.hpp"

namespace pinfs {

struct Address {
  using IpAddress = unsigned long;
  using PortNumber = unsigned short;

  IpAddress ip_address;
  PortNumber port;

  Address() {
  }

  Address(IpAddress ip, PortNumber port)
    : ip_address(ip),
      port(port) {
  }
};

struct Contact {
  Address address;
  Key id;

  Contact() {
  }

  Contact(Address::IpAddress ip, Address::PortNumber port, const Key& id)
    : address(ip, port),
      id(id) {
  }

  std::string toHexString();
};

} //pinfs

#endif //PINFS_CONTACT_HPP
