#ifndef PINFS_ROUTING_TABLE_HPP
#define PINFS_ROUTING_TABLE_HPP

#include "global.h"

#include "Contact.hpp"

#include <array>
#include <vector>
#include <list>

namespace pinfs {

class RoutingTable {
public:
  using Bucket = std::list<Contact>;
  using BucketArray = std::array<Bucket, PINFS_N_BUCKETS>;

public:
  RoutingTable(const pinfs::Contact& myself)
    : me_(myself) {
    buckets_.fill(Bucket());
  }

public:
  void update(const Contact& contact);
  void removeKey(const Key& key);

  std::vector<Contact> findKClosest(const Key& key) const;

  const Bucket& getBucket(const pinfs::Key& key) const;
  const BucketArray& getAllBuckets() const;
  const Key& getKey() const;
  const Contact& getContactToMe() const;

private:
  Contact me_;
  BucketArray buckets_;
};

} //pinfs

#endif //PINFS_ROUTING_TABLE_HPP
