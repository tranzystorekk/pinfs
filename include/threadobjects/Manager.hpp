#ifndef PINFS_THREADOBJECTS_MANAGER_HPP
#define PINFS_THREADOBJECTS_MANAGER_HPP

#include <boost/asio/deadline_timer.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <functional>
#include <unordered_map>

#include "concurrent/SynchronizedQueue.hpp"

#include "threadobjects/MasterSender.hpp"
#include "threadobjects/ControlSynchronizer.hpp"

#include "events/Event.hpp"
#include "messages/Message.hpp"

#include "messages/PingMsg.hpp"
#include "messages/ExistsMsg.hpp"
#include "messages/StoreMsg.hpp"
#include "messages/FindNodeMsg.hpp"
#include "messages/FindValueMsg.hpp"

#include "RoutingTable.hpp"

namespace pinfs {
namespace threadobjects {

//REQUEST INFO
struct RequestInfo {
  messages::Message::Type type;
  pinfs::Key receiverId;
  boost::asio::deadline_timer expiryTime;

  RequestInfo(messages::Message::Type type,
              const pinfs::Key& key,
              boost::asio::io_context& ioc)
    : type(type),
      receiverId(key),
      expiryTime(ioc) {
    expiryTime.expires_from_now(boost::posix_time::minutes(3));
  }
};

// COMMAND SESSION
struct CommandSession {
  enum Type {
             PUT,
             GET,
             NONE
  } type;

  int lastRequestId;
  pinfs::Address nearestNode;
  pinfs::Key nearestDistance;
  pinfs::Key searched;
  int requestsSinceLastImprovement;
  std::string filepath;

  CommandSession()
    : type(Type::NONE),
      nearestDistance(pinfs::Key::FromHexString("ffffffffffffffffffffffffffffffffffffffff")) {
  }

  bool isActive() const {
    return type != Type::NONE;
  }

  void reset() {
    type = Type::NONE;
    nearestDistance = pinfs::Key::FromHexString("ffffffffffffffffffffffffffffffffffffffff");
  }
};

struct Bootstrap {
  enum Given {
              YES,
              NO
  } given;

  pinfs::Address bootstrapAddress;

  Bootstrap()
    : given(Given::NO) {
  }

  Bootstrap(const pinfs::Address& address)
    : given(Given::YES),
      bootstrapAddress(address) {
  }
};

class Manager {
public:
  using EventPtr = std::shared_ptr<events::Event>;
  using UnhandledRequests = std::unordered_map<int, RequestInfo>;
  using IdList = std::unordered_map<std::string, pinfs::Key>;

public:
  Manager(MasterSender& msender, const Contact& me, boost::asio::io_context& ioc, ControlSynchronizer& cs,
          const std::string& path, const Bootstrap& bootstrap)
    : ms_(msender),
      ioc_(ioc),
      cs_(cs),
      expiryDeadline_(ioc),
      pingAllDeadline_(ioc),
      rt_(me),
      nextMsgId_(0),
      storePath_(path),
      bootstrap_(bootstrap) {
  }

public:
  void addToQueue(events::Event* event);

  void executor();

private:
  void init();

  void checkExpiredRequests(const boost::system::error_code& ec);
  void pingAll(const boost::system::error_code& ec);

  void handleStoreRequest(const messages::StoreRequest& storereq);
  void handleFNodeRequest(const messages::FindNodeRequest& fnodereq);
  void handleFValueRequest(const messages::FindValueRequest& fvaluereq);
  void handleExistsRequest(const messages::ExistsRequest& existsreq);

  void handleStoreResponse(const messages::StoreResponse& storeres);
  void handleFNodeResponse(const messages::FindNodeResponse& fnoderes);
  void handleFValueResponse(const messages::FindValueResponse& fvalueres);

  void handleRequest(const messages::Message* msg);
  void handleResponse(const messages::Message* msg);

  void eventLoop(const boost::system::error_code& ec);

private:
  concurrent::SynchronizedQueue<EventPtr> queue_;

  MasterSender& ms_;
  boost::asio::io_context& ioc_;
  ControlSynchronizer& cs_;

  boost::asio::deadline_timer expiryDeadline_;
  boost::asio::deadline_timer pingAllDeadline_;

  pinfs::RoutingTable rt_;
  int nextMsgId_;

  UnhandledRequests unhandled_;
  IdList knownFileIds_;

  CommandSession session_;
  std::string storePath_;
  Bootstrap bootstrap_;
};

} //threadobjects
} //pinfs

#endif //PINFS_THREADOBJECTS_MANAGER_HPP
