#ifndef PINFS_THREADOBJECTS_CONTROLLER_HPP
#define PINFS_THREADOBJECTS_CONTROLLER_HPP

#include <mutex>

#include "events/Event.hpp"
#include "threadobjects/Manager.hpp"
#include "threadobjects/ControlSynchronizer.hpp"

namespace pinfs {
namespace threadobjects {

class Controller {
public:
  Controller(Manager& manager, ControlSynchronizer& cs)
    : manager_(manager),
      cs_(cs) {
  }

  void executor();
private:
  events::Event* parseTextToEvent(const std::string& commandText);
  Manager& manager_;
  ControlSynchronizer& cs_;
};

} //threadobjects
} //pinfs

#endif //PINFS_THREADOBJECTS_CONTROLLER_HPP
