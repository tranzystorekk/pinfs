#ifndef PINFS_THREADOBJECTS_MASTER_SENDER_HPP
#define PINFS_THREADOBJECTS_MASTER_SENDER_HPP

#include <memory>
#include <mutex>
#include <boost/asio.hpp>

#include "Contact.hpp"
#include "messages/Message.hpp"
#include "concurrent/SynchronizedQueue.hpp"

namespace pinfs {
namespace threadobjects {

class AtomicThreadCounter {
  int counter_;
  std::mutex mutex_;
  std::condition_variable cv_;

public:
  AtomicThreadCounter(int initVal = 0)
    : counter_(initVal) {
  }

public:
  void incrementIfLessThan(int val);
  void decrement();
};

struct MsgInfo {
  std::shared_ptr<messages::Message> message;
  pinfs::Address receiver;

  MsgInfo() {
  }

  MsgInfo(messages::Message* m, const pinfs::Address& address)
    : message(m),
      receiver(address) {
  }
};

class MasterSender {
public:
  MasterSender(boost::asio::io_context& ioc)
    : ioc_(ioc) {
  }

public:
  void addToQueue(messages::Message* m, const pinfs::Address& address);

  void executor();

private:
  boost::asio::io_context& ioc_;
  concurrent::SynchronizedQueue<MsgInfo> queue_;
  AtomicThreadCounter tcounter_;
};

void senderWorker(boost::asio::io_context& ioc, std::string msg, Address address, AtomicThreadCounter& counter);

} //threadobjects
} //pinfs

#endif //PINFS_THREADOBJECTS_MASTER_SENDER_HPP
