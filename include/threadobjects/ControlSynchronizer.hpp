#ifndef PINFS_CONTROL_SYNCHRONIZER_HPP
#define PINFS_CONTROL_SYNCHRONIZER_HPP

#include <future>

namespace pinfs {
namespace threadobjects {

class ControlSynchronizer {
public:
  using Future = std::future<std::string>;

public:
  Future getFuture();
  void synchronize(const std::string& str);

private:
  std::promise<std::string> promise_;
};

} //threadobjects
} //pinfs

#endif //PINFS_CONTROL_SYNCHRONIZER_HPP
