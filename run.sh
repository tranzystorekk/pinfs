#!/bin/bash

get_my_ip()
{

	ip_addresses_v1="$(hostname -I 2>/dev/null)" # For no Arch linux sytems
	if [ "$?" -eq 0 ]
	then
		ip_addresses="$ip_addresses_v1"
	else
		ip_addresses="$(hostname -i 2>/dev/null)"
	fi
	IFS=' ' read -r -a array <<< "$ip_addresses"
	echo -e "Which ip address you want to use?\n"
	c=1
	while [ $c -le "${#array[@]}" ]
	do
		echo "$c) ${array[$c-1]}"
		(( c++ ))
	done

	echo -e "\nType number"

	read num

    if [ "$num" == "" ]
    then
        num=1
    fi

	my_ip_address="${array[$num-1]}"

	echo "Write port (if you want to use default press Enter)"

	read port

	if [ "$port" != "" ]
	then
	    my_ip_address="$my_ip_address"":""$port"
	fi

	echo "$my_ip_address"

}

get_dest_path()
{
    echo "Write path to destination"

	read dest_path

	if [ "$dest_path" == "" ]
	then
	    dest_path="."
	fi

	echo "Destination folder: $dest_path"

}

get_bootstrap_ip_address()
{
    echo "Write bootstrap ip_address[:port]"

	read bootstrap

	echo "Bootstrap address: $bootstrap"

}

my_ip_address=""
dest_path=""
bootstrap=""

help_string="\nThis is simple running script which run pinfs, This script with no params gets users ip addresses from which you choose one to identify yourself with,
next asks about a port for this identification (if you will type nothing then port will be default),
then chooses destination path for file storage and finally gets bootstrap ip address[:port],
when you use this script with params each one is as follows: ip_address[:port], destination_path, bootstrap_ip_address[:port],
you can run script with either one, two or three arguments.\nDefault port: 50000\n
┌--------------┐
|PINFS MAN PAGE|
└--------------┘\n"

if [ "$1" == "-h" ] || [ "$1" == "--help" ]
then
    echo -e "$help_string"

    ./bin/debug/prog --help
    exit 0
fi

if [ $# -lt 1 ]
then
    get_my_ip
    get_dest_path
    get_bootstrap_ip_address


elif [ $# -lt 2 ]
then
	my_ip_address="$1"
	get_dest_path
	get_bootstrap_ip_address

elif [ $# -lt 3 ]
then
	my_ip_address="$1"
	dest_path="$2"
	get_bootstrap_ip_address
else
	my_ip_address="$1"
	dest_path="$2"
	bootstrap="$3"
fi


if [ "$bootstrap" == "" ]
then
    echo -e "Starting pinfs with ip address: $my_ip_address\nDestination folder: $dest_path\nWithout bootstrap ip address."
    ./bin/debug/prog -a "$my_ip_address" -p "$dest_path"
else
    echo -e "Starting pinfs with ip address: $my_ip_address\nDestination folder: $dest_path\nBootstrap ip address: $bootstrap"

    ./bin/debug/prog -b "$bootstrap" -a "$my_ip_address" -p "$dest_path"
fi




